/*
    This file is generated and updated by Sencha Cmd. You can edit this file as
    needed for your application, but these edits will have to be merged by
    Sencha Cmd when it performs code generation tasks such as generating new
    models, controllers or views and when running "sencha app upgrade".

    Ideally changes to this file would be limited and most work would be done
    in other places (such as Controllers). If Sencha Cmd cannot merge your
    changes and its generated code, it will produce a "merge conflict" that you
    will need to resolve manually.
*/

// Fix for scroll 
Ext.override(Ext.util.SizeMonitor, {
constructor: function (config) {
    var namespace = Ext.util.sizemonitor;
    return new namespace.Scroll(config);
}
});
Ext.override(Ext.util.PaintMonitor, {
constructor: function (config) {
    return new Ext.util.paintmonitor.CssAnimation(config);
}
});

Ext.application({
    name: 'ContactApp',

    models: [
        'Category', 'Company', 'ContactType', 'ContactSubType', 'User', 'Contact', 'City', 'UserFb'
    ],

    stores: [
        'Categories','Companies', 'ContactTypes', 'ContactSubTypes', 'Users', 'Cities'
    ],

    requires: [
        'Ext.MessageBox', 'ContactApp.util.Pushwoosh'
    ],

    views: [
        'Main', 'Login', 'Register', 'Category', 'Company', 'Contacto', 'CapturePicture', 'Share', 'Info', 'Profile', 'History', 'ContactDetail', 'Recovery', 'CreateCategory'
    ],

    controllers: [
        'Auth', 'User', 'Category', 'Company', 'Contacto', 'Share', 'Facebook'
    ],

    icon: {
        '57': 'resources/icons/Icon.png',
        '72': 'resources/icons/Icon~ipad.png',
        '114': 'resources/icons/Icon@2x.png',
        '144': 'resources/icons/Icon~ipad@2x.png'
    },

    isIconPrecomposed: true,

    startupImage: {
        '320x460': 'resources/startup/320x460.jpg',
        '640x920': 'resources/startup/640x920.png',
        '768x1004': 'resources/startup/768x1004.png',
        '748x1024': 'resources/startup/748x1024.png',
        '1536x2008': 'resources/startup/1536x2008.png',
        '1496x2048': 'resources/startup/1496x2048.png'
    },

    // BASE_URL: 'http://192.168.1.31:8080/contactappserver/',
    // BASE_URL: 'http://contactapp-crowdcoding.rhcloud.com/',

    launch: function() {
        
        
        // Destroy the #appLoadingIndicator element
        Ext.fly('appLoadingIndicator').destroy();

        // Initialize the main view
        Ext.Viewport.add(Ext.create('ContactApp.view.Login'));

    },

    onUpdated: function() {
        Ext.Msg.confirm(
            "Application Update",
            "This application has just successfully been updated to the latest version. Reload now?",
            function(buttonId) {
                if (buttonId === 'yes') {
                    window.location.reload();
                }
            }
        );
    }
});

