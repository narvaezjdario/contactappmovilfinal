Ext.define('ContactApp.model.Contact', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            { name: 'class', type: 'string'},
            { name: 'id', type: 'int'},
            { name: 'contactType', type: 'int'},
            { name: 'contactSubType', type:'int'},
            { name: 'description', type: 'string' },
            { name: 'reference', type: 'string'},
            { name: 'address', type: 'string'},
            { name: 'image', type: 'string'},
            { name: 'dateCreated', type: 'date'},
            { name: 'lastUpdated', type: 'date'},
            //campos para que sean visualizados en el hisotrial
            { name: 'nameContactType', type: 'string'},
            { name: 'nameCompany', type: 'string'},
            { name: 'state', type: 'string'}
            
        ],

        validations: [
            {
                field: 'description',
                type: 'presence',
                message: 'La descripción es requerida'
            },
            {
                field: 'reference',
                type: 'presence',
                message: 'El número de referencia es requerido'
            },
            {
                field: 'address',
                type: 'presence',
                message: 'Es requerido la dirección de domicilio'
            },
            {
                field: 'contactType',
                type: 'presence',
                message: 'Es requerido el tipo de contacto'
            },
            {
                field: 'contactSubType',
                type: 'presence',
                message: 'Es requerido el tipo de subcontacto'
            }
            
        ]

    }
});
