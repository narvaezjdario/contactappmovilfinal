Ext.define('ContactApp.model.ContactSubType', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            { name: 'class', type: 'string'},
            { name: 'id', type: 'int'},
            { name: 'name', type: 'string' },
            { name: 'status', type: 'string' },
            { name: 'dateCreated', type: 'date'},
            { name: 'lastUpdated', type: 'date'},
            { name: 'contactTypeId', type: 'int' }
        ]
    }
});
