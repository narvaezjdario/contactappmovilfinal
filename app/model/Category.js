Ext.define('ContactApp.model.Category', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            { name: 'class', type: 'string'},
            { name: 'id', type: 'int'},
            { name: 'name', type: 'string' },
            { name: 'imageUrl', type: 'string' },
            { name: 'code', type: 'string' },
            { name: 'referenceLabel', type: 'string'},
            { name: 'dateCreated', type: 'date'},
            { name: 'lastUpdated', type: 'date'}
        ],
        hasMany: {
            model: 'ContactApp.model.Company', 
            name: 'companies',
            primaryKey: 'id',
            foreignKey: 'categoryId'
        }

    }
});
