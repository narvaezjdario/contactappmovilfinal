Ext.define('ContactApp.model.Company', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            { name: 'class', type: 'string'},
            { name: 'id', type: 'int'},
            { name: 'imageUrl', type: 'string'},
            { name: 'name', type: 'string' },
            { name: 'contactName', type: 'string' },
            { name: 'contactEmail', type: 'string' },
            { name: 'dateCreated', type: 'date'},
            { name: 'lastUpdated', type: 'date'},
            { name: 'categoryId', type: 'int'},
            { name: 'oversightId', type: 'int'}
        ],
        belongsTo: { 
            model: 'ContactApp.model.Category', 
            name: 'category' , 
            primaryKey: 'id',
            foreignKey: 'categoryId'
        },
    }
});
