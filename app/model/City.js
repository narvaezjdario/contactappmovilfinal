Ext.define('ContactApp.model.City', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            { name: 'class', type: 'string'},
            { name: 'id', type: 'int'},
            { name: 'name', type: 'string' },
            { name: 'shortName', type: 'string' }
        ]
    }
});
