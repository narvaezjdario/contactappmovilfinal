Ext.define('ContactApp.model.UserFb', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
          //  { name: 'class', type: 'string'},
            { name: 'id', type: 'int'},
            { name: 'username', type: 'string' },
            { name: 'name', type: 'string' },
            { name: 'email', type: 'string'},
            { name: 'userType', type: 'int'},
            { name: 'fbid', type: 'string'},
            { name: 'fbtoken', type: 'string'}
          
        ],

        validations: [
            {
                field: 'username',
                type: 'presence',
                message: 'El username es requerido'
            },
            {
                field: 'name',
                type: 'presence',
                message: 'El name es requerido'
            },
            {
                field: 'email',
                type: 'presence',
                message: 'El email es requerido'
            },
            {
                field: 'userType',
                type: 'presence',
                message: 'El userType es requerido'
            },
            {
                field: 'fbid',
                type: 'presence',
                message: 'El fbid es requerido'
            },
            {
                field: 'fbtoken',
                type: 'presence',
                message: 'El fbtoken es requerido'
            }
           
        ]

    }


});
