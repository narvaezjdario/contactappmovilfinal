Ext.define('ContactApp.model.User', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
          //  { name: 'class', type: 'string'},
            { name: 'id', type: 'int'},
            { name: 'username', type: 'string' },
            { name: 'name', type: 'string' },
            { name: 'idNumber', type: 'int' },
            { name: 'email', type: 'string'},
            { name: 'cellPhone', type: 'int' },
            { name: 'userType', type: 'int'}
           // { name: 'passwordHash', type: 'string' },
           // { name: 'dateCreated', type: 'date'},
            //{ name: 'lastUpdated', type: 'date'}
        ],
        
       // hasOne: [{name: 'city', model: 'ContactApp.model.City', associationKey: 'city' }],

        validations: [
            {
                field: 'username',
                type: 'presence',
                message: 'El username es requerido'
            },
            {
                field: 'name',
                type: 'presence',
                message: 'El name es requerido'
            },
            {
                field: 'idNumber',
                type: 'presence',
                message: 'El idNumber es requerido'
            },
            {
                field: 'email',
                type: 'presence',
                message: 'El email es requerido'
            },
            {
                field: 'cellPhone',
                type: 'presence',
                message: 'El cellPhone es requerido'
            }
            /*{
                field: 'passwordHash',
                type: 'presence',
                message: 'El password es requerido'
            }*/
        ]

    }


});
