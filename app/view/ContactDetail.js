var myTpl = new Ext.XTemplate(
                    '<tpl for=".">',
                        '<h1>{nameCompany}</h1>',
                        '<p>{description}</p>',
                        '<p>{reference}</p>',
                        '<p>{nameContactType}</p>',
                        '<p>{state}</p>',
                        '<p>{dateCreated}</p>',
                    '</tpl>'
                );

Ext.define("ContactApp.view.ContactDetail",{
    extend: 'Ext.Container',
    xtype: 'contactdetail',
    config: {
        layout: 'vbox',
        items: [
            {
                xtype: 'toolbar',
                docked: 'top',
                title: 'Detalle Contacto',
                items:[
                    {
                        xtype: 'button',
                        iconCls: 'arrow_left',
                        ui: 'plain',
                        itemId: 'backBtn'
                    }
                ]

            },
            {
                flex: 1,
                xtype: 'component',
                itemId: 'infoContact',
                tpl: myTpl
            }
        ],
        
        listeners: [
            {
                delegate: '#backBtn',
                event: 'tap',
                fn: 'onBackBtn'
            }
        ]
    },
    
     onBackBtn: function(){
        var me = this;
         //Este evento es manejado por controlador Share
        me.fireEvent('onBackBtn');
    },
    
    refreshInfo: function(contact){
        
        console.log('entro a refresco');
        console.log(contact);
        var infoContact = this.down('#infoContact');
        infoContact.setRecord(contact);
    }
    

});