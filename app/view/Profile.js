Ext.define('ContactApp.view.Profile',{
    extend: 'Ext.Container',
    xtype: 'profile',
    requires: ['Ext.field.Email','Ext.field.Number'],
    config: {
        layout:'vbox',
        items:[
            {
                xtype: 'toolbar',
                docked: 'top',
                title: 'ContactApp',
                items:[
                    {
                        xtype: 'button',
                        iconCls: 'arrow_left',
                        ui: 'plain',
                        itemId: 'backBtn'
                    }
                ]
            },
            {
                xtype: 'label', html: 'Error en el formulario', 
                hidden: true, //Campo oculto, para errores en el login 
                style: 'color:#990000;margin:5px 0px;',
                itemId: 'failedLabel'
            },
            {
                xtype: 'label', html: 'Envio Exitoso', 
                hidden: true, //Campo oculto, para errores en el login 
                style: 'color:#009900;margin:5px 0px;',
                itemId: 'okLabel'
            },
            {
                xtype: 'formpanel',
                flex: 1,
                itemId: 'formEdit',
                items:[
                    {
                        xtype: 'fieldset',
                        items:[
                            {
                                xtype:'textfield',
                                name: 'username',
                                labelWrap: true,
                                label: 'Nombre de Usuario',
                                placeHolder: 'Nombre de usuario',   
                                required: true,
                                readOnly: true
                            },
                            {
                                xtype:'textfield',
                                name: 'name',
                                labelWrap: true,
                                label: 'Nombre',
                                placeHolder: 'Nombre',   
                                required: true,
                                readOnly: true
                            },
                            {
                                xtype: 'emailfield',
                                name: 'email',
                                labelWrap: true,
                                label: 'Email',
                                placeHolder: 'user@dominio.com',   
                                required: true,
                                readOnly:true
                            },
                            {
                                xtype:'numberfield',
                                name: 'idNumber',
                                labelWrap: true,
                                label: 'Cedula',
                                placeHolder: 'Numero de identificación',   
                                required: true
                            },
                            {
                                xtype: 'numberfield',
                                name: 'cellPhone',
                                labelWrap: true,
                                label: 'Celular',
                                placeHolder: '3150000000',   
                                required: true
                            },
                            {
                                name: 'city',
                                xtype: 'selectfield',
                                label: 'Ciudad',
                                usePicker: true,
                                valueField: 'id',
                                displayField: 'name',
                                store: 'Cities',
                                autoSelect: false,
                                placeHolder: 'Seleccione una Ciudad',
                                required:true
                            }
                        ]
                    },
                    {
                        xtype: 'toolbar',
                        layout: {
                            pack: 'center' 
                        },
                        ui: 'plain',
                        items:[
                            {
                                xtype: 'button', text: 'Guardar', ui: 'action', itemId: 'guardarBtn'
                            }
                        ]
                    }
                ]
            }

        ],
        listeners:[
            {
                delegate: '#guardarBtn',
                event: 'tap',
                fn: 'onGuardarBtnTap'
            },
            {
                delegate: '#backBtn',
                event: 'tap',
                fn: 'onBackBtn'
            }
        ]
    },

    initialize: function(){
        var me = this;
        var userStore = Ext.getStore('Users');
        var user = userStore.findRecord('id', localStorage.getItem("userId"));
        var formulario = me.down('#formEdit');
        formulario.setRecord(user);

    },

    onBackBtn: function(){
        var me = this;
        //Este evento es manejado por controlador Share
        me.fireEvent('onBackBtn');
    },

    onGuardarBtnTap: function(){
        var me = this;
        var formulario = me.down('#formEdit');
        var usuario = formulario.getValues();
        var failedLabel = me.down('#failedLabel');
        failedLabel.hide();
        var okLabel = me.down('#okLabel');
        okLabel.hide();

        var task = Ext.create('Ext.util.DelayedTask', function () {
            failedLabel.setHtml('');
            okLabel.setHtml('');
            me.fireEvent('editarUsuario', me, usuario);

        });
        task.delay(500);
    },

    showFailedMessage: function(mensaje) {
        var label = this.down('#failedLabel');
        label.setHtml(mensaje);
        label.show();
    },

    showOkMessage: function(mensaje) {
        var label = this.down('#okLabel');
        label.setHtml(mensaje);
        label.show();
        var task = Ext.create('Ext.util.DelayedTask', function () {
            label.setHtml('');
            label.hide();

        });
        task.delay(1000);


    }

});