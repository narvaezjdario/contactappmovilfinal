Ext.define("ContactApp.view.History",{
    extend: 'Ext.Container',
    xtype: 'history',
    requires: [
        'Ext.dataview.List'
    ],
    config: {
        layout: {
            type: 'fit'
        },
        items: [
            {
                xtype: 'toolbar',
                docked: 'top',
                title: 'Historial Contactos',
                items:[
                    {
                        xtype: 'button',
                        iconCls: 'arrow_left',
                        ui: 'plain',
                        itemId: 'backBtn'
                    }
                ]

            },
            {
                xtype: 'list',
                itemTpl: new Ext.XTemplate('<div>'+
                                           '<h1>{nameCompany}</h1>'+
                                           '<h2>{nameContactType}</h2>'+
                                           '<p>{state}</p>'+
                                           '<p>{dateCreated}</p>'+
                                           '<div>'),
                itemId: 'listHistory'
            }
        ],

        listeners: [
            {
                delegate: '#backBtn',
                event: 'tap',
                fn: 'onBackBtn'
            },
            {
                delegate: '#listHistory',
                event: 'itemtap',
                fn: 'onContactSelected'
            }
        ]
    },
    initilize: function(){
        this.refreshList();
        
        
    },

    onBackBtn: function(){
        var me = this;
        //Este evento es manejado por controlador Share
        me.fireEvent('onBackBtn');
    },
    
    refreshList: function(storeTemporal){
        console.log('llego a la vista');
        console.log(storeTemporal);
        var me = this;
        var listHistory = me.down('#listHistory');
        listHistory.setStore(storeTemporal);
        
        if(storeTemporal.data.length===0){
            listHistory.setHtml("No se ha encontrado informaci&oacute;n");
        }
        
    },
    
    onContactSelected: function(view, index, target, contact){
        this.fireEvent('onContactSelected', contact);
    }

});