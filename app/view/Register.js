Ext.define('ContactApp.view.Register',{
    extend: 'Ext.Container',
    xtype: 'register',
    requires: ['Ext.field.Email','Ext.field.Number', 'Ext.field.Select'],
    config: {
        layout:'vbox',
        items:[
            {
                xtype: 'toolbar',
                docked: 'top',
                title: 'ContactApp',
                items:[
                    {
                        xtype: 'button',
                        iconCls: 'arrow_left',
                        ui: 'plain',
                        itemId: 'backBtn'
                    }
                ]
            },
            {
                xtype: 'label', html: 'Error en el formulario', 
                hidden: true, //Campo oculto, para errores en el login 
                style: 'color:#990000;margin:5px 0px;',
                itemId: 'failedLabel'
            },
            {
                xtype: 'formpanel',
                flex: 1,
                itemId: 'formRegister',
                items:[
                    {
                        xtype: 'fieldset',
                        items:[
                            {
                                xtype:'textfield',
                                name: 'username',
                                labelWrap: true,
                                label: 'Nombre de Usuario',
                                placeHolder: 'Nombre de usuario',   
                                required: true
                            },
                            {
                                xtype:'textfield',
                                name: 'name',
                                labelWrap: true,
                                label: 'Nombre',
                                placeHolder: 'Nombre',   
                                required: true
                            },
                            {
                                xtype:'numberfield',
                                name: 'idNumber',
                                labelWrap: true,
                                label: 'Cedula',
                                placeHolder: 'Numero de identificaci&oacute;n',   
                                required: true
                            },
                            {
                                xtype:'passwordfield',
                                name: 'passwordHash',
                                labelWrap: true,
                                label: 'Password',
                                placeHolder: 'MiPassword',   
                                required: true
                            },
                            {
                                xtype:'passwordfield',
                                name: 'confirmPassword',
                                labelWrap: true,
                                label: 'Confirmar Password',
                                placeHolder: 'Confirmar Password',   
                                required: true
                            },
                            {
                                xtype: 'emailfield',
                                name: 'email',
                                labelWrap: true,
                                label: 'Email',
                                placeHolder: 'user@dominio.com',   
                                required: true
                            },
                            {
                                xtype: 'numberfield',
                                name: 'cellPhone',
                                labelWrap: true,
                                label: 'Celular',
                                placeHolder: '3150000000',   
                                required: true
                            },
                            {
                                name: 'city',
                                xtype: 'selectfield',
                                label: 'Ciudad',
                                usePicker: true,
                                valueField: 'id',
                                displayField: 'name',
                                store: 'Cities',
                                autoSelect: false,
                                placeHolder: 'Seleccione una Ciudad',
                                required:true
                            }
                        ]
                    },
                    {
                        xtype: 'toolbar',
                        layout: {
                            pack: 'center' 
                        },
                        ui: 'plain',
                        items:[
                            {
                                xtype: 'button', text: 'Registrarme', ui: 'action', itemId: 'guardarBtn'
                            }
                        ]
                    }
                ]
            }
            
        ],
        listeners:[
            {
                delegate: '#guardarBtn',
                event: 'tap',
                fn: 'onGuardarBtnTap'
            },
            {
                delegate: '#backBtn',
                event: 'tap',
                fn: 'onBackBtn'
            }
        ]
    },
    
    onBackBtn: function(){
        var me = this;
        me.fireEvent('onBackLogin');
    },
    
    onGuardarBtnTap: function(){
        var me = this;
        var formulario = me.down('#formRegister');
        var usuario = formulario.getValues();
        var failedLabel = me.down('#failedLabel');
        failedLabel.hide();
        
        var task = Ext.create('Ext.util.DelayedTask', function () {
            failedLabel.setHtml('');
            me.fireEvent('crearUsuario', me, usuario);
           // formulario.reset();
        });
        task.delay(500);
    },
    
    showFailedMessage: function(mensaje) {
        var label = this.down('#failedLabel');
        label.setHtml(mensaje);
        label.show();
    },
    
    clearForm: function(){
        var formulario = this.down('#formRegister');
        formulario.reset();
    }
});