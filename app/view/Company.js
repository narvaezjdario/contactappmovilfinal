Ext.define("ContactApp.view.Company",{
    extend: 'Ext.Container',
    xtype: 'company',
    requires: [
        'Ext.dataview.List'
    ],
    config: {
        layout: {
            type: 'fit'
        },
        items: [
            {
                xtype: 'toolbar',
                docked: 'top',
                title: 'Empresas',
                items:[
                    {
                        xtype: 'button',
                        iconCls: 'arrow_left',
                        ui: 'plain',
                        itemId: 'backBtn'
                    }
                ]

            },
            {
                xtype: 'list',
                itemTpl: new Ext.XTemplate( '<div class="item-companies">'+
                                           '<div class="compbackground"><h3> {name}</h3></div>'+ 
                                           '</div>'
                                          ),
                itemId: 'listCompany',
                cls: 'mGridlist'
            }
        ],

        listeners: [
            {
                delegate: '#listCompany',
                event: 'itemtap',
                fn: 'onCompanySelected'
            },
            {
                delegate: '#backBtn',
                event: 'tap',
                fn: 'onBackBtn'
            }
        ]

    },

    onCompanySelected:  function(view, index, target, company){
        this.fireEvent('onCompanySelected', company);
    },


    refreshCompanies: function(companies){
        console.log('entro a refresh');
        console.log(companies.data.length);
        var listCompany = this.down('#listCompany');
        listCompany.setStore(companies);
        
        if(companies.data.length===0){
            listCompany.setHtml("No se ha encontrado informaci&oacute;n");
        }

    },

    onBackBtn: function(){
        var me = this;
        me.fireEvent('onBackBtn');
    }

});