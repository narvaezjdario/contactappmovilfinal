Ext.define("ContactApp.view.Login",{
    extend: 'Ext.form.Panel',
    xtype: 'login',
    requires: ['Ext.form.FieldSet', 'Ext.form.Password', 'Ext.Label', 'Ext.util.DelayedTask', 'Ext.Img'],
    config: {
        title: "Login",
        layout: 'vbox',
        scrollable: true,
        items: [
            {
                xtype: 'image',
                cls: 'backHeader'
            },
            {
                xtype: 'label', html: 'Credenciales erroneas', 
                hidden: true, //Campo oculto, para errores en el login 
                style: 'color:#990000;margin:5px 0px;',
                itemId: 'failedLabel'
            },
            {
                layout: 'vbox',
                xtype: 'fieldset',
                cls: 'fieldsetlogin',
                items: [
                    {
                        xtype: 'label',
                        baseCls: 'mLabel',
                        html: 'Usuario'
                    },
                    {
                        xtype: 'textfield', placeHolder: 'Username',
                        name: 'userNameTextField',
                        required: true,
                        itemId: 'usernameField',
                        cls: 'mInputForm'

                    },
                    {
                        xtype: 'label',
                        baseCls: 'mLabel',
                        html: 'Contrase&ntilde;a'
                    },
                    {
                        xtype: 'passwordfield', placeHolder: 'Password',
                        name: 'passwordTextField',
                        required: true,
                        itemId: 'passwordField',
                        cls: 'mInputForm'
                    },
                    {
                        xtype: 'button',
                        text: 'Entrar',
                        itemId: 'loginBtn',
                        cls: 'mButton'

                    }
                ]
            },
            {
                xtype: 'button',
                text: '&iquest;Nuevo usuario?',
                itemId: 'registerBtn',
                cls: 'mButton2'

            },
            {
                xtype: 'button',
                text: 'Conectar con Facebook',
                itemId: 'facebookBtn',
                cls: 'mButtonFacebook'
            },
            {
                xtype: 'button',
                text: '&iquest;Olvido su contraseña?',
                itemId: 'recoverBtn',
                cls: 'mButton2'

            },
            {
                xtype: 'label',
                cls: 'mLabel3',
                html: 'Peticiones, Quejas y Reclamos'
            },
            {
                xtype: 'label',
                cls: 'mLabel2',
                html: 'SOBRE LO QUE QUIERAS',
            }

        ],

        listeners: [
            {
                delegate: '#loginBtn',
                event: 'tap',
                fn: 'onLoginBtnTap'
            },
            {
                delegate: '#registerBtn',
                event: 'tap',
                fn: 'onRegisterBtnTap'
            },
            {
                delegate: '#recoverBtn',
                event: 'tap',
                fn: 'onRecoverBtnTap'
            },
            {
                delegate: '#facebookBtn',
                event: 'tap',
                fn: 'onFacebookBtnTap'

            }
        ]
    },

    onLoginBtnTap: function(){
        var vista = this;
        var usernameField = vista.down('#usernameField');
        var passwordField = vista.down('#passwordField');
        var failedLabel = vista.down('#failedLabel');
        failedLabel.hide();

        var username = usernameField.getValue();
        var password = passwordField.getValue();
        var userType = 0; //usuario normal
        var task = Ext.create('Ext.util.DelayedTask', function () {
            failedLabel.setHtml('');
            vista.fireEvent('onLoginBtnTap', vista, username, password, userType);
            usernameField.setValue('');
            passwordField.setValue('');
        });

        task.delay(500);
    },

    onRegisterBtnTap: function(){
        var me = this;
        me.fireEvent('onRegisterBtnTap');

    },

    showFailedMessage: function(mensaje) {
        var label = this.down('#failedLabel');
        label.setHtml(mensaje);
        label.show();
    },

    onRecoverBtnTap: function(){
        var me = this;
        me.fireEvent('onRecoverBtnTap');
    },

    onFacebookBtnTap: function(){

        var me = this;
        me.fireEvent('onFacebookBtnTap', me);
        
    }


});