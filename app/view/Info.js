Ext.define("ContactApp.view.Info",{
    extend: 'Ext.Container',
    xtype: 'info',
    config: {
        items: [
            {
                xtype: 'toolbar',
                docked: 'top',
                title: 'Info ContacApp',
                items:[
                    {
                        xtype: 'button',
                        iconCls: 'arrow_left',
                        ui: 'plain',
                        itemId: 'backBtn'
                    }
                ]

            },
            {
                xtype: 'container',
                html:[
                    '<p>',
                    'Información sobre ContactApp, creada por AgilCon ',
                    '</p>'

                ].join('')
            }
        ],
        
        listeners: [
            {
                delegate: '#backBtn',
                event: 'tap',
                fn: 'onBackBtn'
            }
        ]
    },
    
     onBackBtn: function(){
        var me = this;
         //Este evento es manejado por controlador Share
        me.fireEvent('onBackBtn');
    }
    

});