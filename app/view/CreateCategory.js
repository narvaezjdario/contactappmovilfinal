Ext.define("ContactApp.view.CreateCategory",{
    extend: 'Ext.Container',
    xtype: 'createcategory',
    config: {
        layout:'vbox',
        items: [
            {
                xtype: 'toolbar',
                docked: 'top',
                title: 'Nueva Categoría',
                items:[
                    {
                        xtype: 'button',
                        iconCls: 'arrow_left',
                        ui: 'plain',
                        itemId: 'backBtn'
                    }
                ]

            },
            {
                xtype: 'label', html: 'Envio Exitoso', 
                hidden: true, //Campo oculto, para errores en el login 
                style: 'color:#009900;margin:5px 0px;',
                itemId: 'messageLabel'
            },
            {
                xtype: 'formpanel',
                flex: 1,
                itemId: 'formCreateCategory',
                items:[
                    {
                        xtype: 'textfield',
                        name: 'name',
                        labelWrap: true,
                        label: 'Nombre',
                        placeHolder: 'Nombre de la categoría',   
                        required: true,
                        itemId: 'name'
                    },
                    {
                        xtype: 'toolbar',
                        layout: {
                            pack: 'center' 
                        },
                        ui: 'plain',
                        items:[
                            {
                                xtype: 'button', text: 'Enviar', ui: 'action', itemId: 'createBtn'
                            }
                        ]
                    }

                ]
            }
        ],

        listeners: [
            {
                delegate: '#backBtn',
                event: 'tap',
                fn: 'onBackBtn'
            },
            {
                delegate: '#createBtn',
                event: 'tap',
                fn: 'onCreateBtn'
            }
            
        ]
    },

    onBackBtn: function(){
        var me = this;
        //Evento manejado por el controlador Share.
        me.fireEvent('onBackBtn');
    },
    
    onCreateBtn: function(){
        var me = this;
        var name = me.down('#name').getValue();
        //Controlador Category
        me.fireEvent('onCreateCategory', name);
    },
    
    showMessage: function(mensaje) {
        var label = this.down('#messageLabel');
        label.setHtml(mensaje);
        label.show();
         var task = Ext.create('Ext.util.DelayedTask', function () {
            label.setHtml('');
            label.hide();
        });
        task.delay(1500);
        
    }
    
});