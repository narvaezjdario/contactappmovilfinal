Ext.define("ContactApp.view.Category",{
    extend: 'Ext.Container',
    xtype: 'category',
    requires: [
        'Ext.Menu', 'Ext.dataview.DataView'
    ],
    config: {
        layout: {
            type: 'card'
        },
        items: [
            {
                xtype: 'toolbar',
                docked: 'top',
                title: 'Categor&iacute;as',
                items: [
                    {
                        xtype: 'button',
                        id: 'listButton',
                        iconCls: 'list',
                        ui: 'plain',
                        handler: function(){
                            if(Ext.Viewport.getMenus().left.isHidden()){
                                Ext.Viewport.showMenu('left');
                            }
                            else
                            {
                                Ext.Viewport.hideMenu('left');
                            }
                        }
                    }
                ]
            },
            {
                xtype: 'dataview',
                store: 'Categories',
                itemTpl: new Ext.XTemplate( '<div class="item-categorias">'+
                                           '<div class="catbackground" style="background-image:url(http://192.168.0.15:8080/contactappserver{imageUrl});"><h3>{name}</h3></div>'+ 
                                           '</div>'
                                          ),
                itemId: 'listCategory'
            }
        ],

        listeners: [
            {
                delegate: '#listCategory',
                event: 'itemtap',
                fn: 'onCategorySelected'
            }

        ]

    },

    initialize: function(){
        Ext.Viewport.setMenu(this.createMenu(),{
            side: 'left',
            reveal: true
        });

        Ext.getStore('Categories').load();
        Ext.getStore('Companies').load();
    },

    createMenu: function(){
        var me = this;
        var menu = Ext.create('Ext.Menu', {
            width: 250,
            scrollable: 'vertical',
            items: [
                {
                    xtype: 'button',
                    text: 'Mi Perfil',
                    handler: function(){
                        me.onProfile()
                    }
                },
                {
                    xtype: 'button',
                    text: 'Mi Historial',
                    handler: function(){
                        me.onHistory()
                    }
                },
                {
                    xtype: 'button',
                    text: 'Sobre la App',
                    handler: function(){
                        me.onInfo()
                    }
                },
                {
                    xtype: 'button',
                    ui: 'decline',
                    text: 'Cerrar Sesi&oacute;n',
                    handler: function(){
                        me.onCerrar()
                    }
                }
            ]
        });

        return menu;
    },

    onCategorySelected:  function(view, index, target, category){
        this.fireEvent('onCategorySelected', category);
    },
    
    onCerrar: function(){
        var me = this;
        //Manejado por el controlador Auth
        Ext.Viewport.hideMenu('left');
        me.fireEvent('onCerrar', me);
        
    },
    
    onInfo: function(){
        var me = this;
        me.fireEvent('onInfo');
    },
    
    onProfile: function(){
        var me = this;
        me.fireEvent('onProfile');
    },
    
    onHistory: function(){
        var me = this;
        me.fireEvent('onHistory');
    }

});