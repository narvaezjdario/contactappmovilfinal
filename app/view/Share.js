Ext.define("ContactApp.view.Share",{
    extend: 'Ext.Container',
    xtype: 'share',
    config: {

        items: [
            {
                xtype: 'toolbar',
                docked: 'top',
                title: 'ContacApp',
                items:[
                    {
                        xtype: 'button',
                        iconCls: 'arrow_left',
                        ui: 'plain',
                        itemId: 'backBtn'
                    }
                ]

            },
            {
                xtype: 'container',
                html:[
                    '<p>',
                    'La	información	ha	sido enviada ',	
                    'al	centro	de	contacto de ',	
                    'XNombreEmpresaX.',
                    '</p>',
                    '<br>',
                    '<p>',
                    '¿Desea compartirla?',
                    '</p>'

                ].join('')
            },
            {
                xtype: 'toolbar',
                layout: {
                    pack: 'center' 
                },
                ui: 'plain',
                items:[
                    {
                        xtype: 'button', text: 'Compartir', ui: 'action',itemId: 'shareBtn'
                    },
                    {
                        xtype: 'button', text: 'Cerrar', ui: 'normal'
                    }
                ]
            }

        ],

        listeners: [
            {
                delegate: '#backBtn',
                event: 'tap',
                fn: 'onBackBtn'
            },
            {
                delegate: '#shareBtn',
                event: 'tap',
                fn: 'onShareBtn'
            }
        ]
    },

    onBackBtn: function(){
        var me = this;
        me.fireEvent('onBackBtn');
    },

    onShareBtn: function(){
        facebookConnectPlugin.showDialog( 
            {
                method: "feed",
                picture:'http://192.168.0.15:8080/contactappserver/dbContainerImage/index?imageId=2',
                name:'ContactApp',
                message:'ContactApp',    
                caption: 'ContactApp',
                description: 'Acabo de crear un contacto con ContactApp'
            }, 
            function (response) { alert("Se ha publicado correctamente en tu muro"); },
            function (response) { alert("Hubo problemas en la publicacion"); });
    }
});