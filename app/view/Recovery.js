Ext.define('ContactApp.view.Recovery',{
    extend: 'Ext.Container',
    xtype: 'recovery',
    requires: ['Ext.field.Email','Ext.field.Number'],
    config: {
        layout:'vbox',
        items:[
            {
                xtype: 'toolbar',
                docked: 'top',
                title: 'ContactApp',
                items:[
                    {
                        xtype: 'button',
                        iconCls: 'arrow_left',
                        ui: 'plain',
                        itemId: 'backBtn'
                    }
                ]
            },
            {
                xtype: 'label', html: 'Error en el formulario', 
                hidden: true, //Campo oculto, para errores en el login 
                style: 'color:#009900;margin:5px 0px;',
                itemId: 'messageLabel'
            },
            {
                xtype: 'formpanel',
                flex: 1,
                itemId: 'formRecovery',
                items:[
                    {
                        xtype: 'emailfield',
                        name: 'email',
                        labelWrap: true,
                        label: 'Email',
                        placeHolder: 'user@dominio.com',   
                        required: true,
                        itemId: 'emailId'
                    },
                    {
                        xtype: 'toolbar',
                        layout: {
                            pack: 'center' 
                        },
                        ui: 'plain',
                        items:[
                            {
                                xtype: 'button', text: 'Enviar', ui: 'action', itemId: 'recuperarBtn'
                            }
                        ]
                    }

                ]
            }
        ],

        listeners: [
            {
                delegate: '#recuperarBtn',
                event: 'tap',
                fn: 'onRecoveryBtn'
            },
            {
                delegate: '#backBtn',
                event: 'tap',
                fn: 'onBackBtn'
            }
        ]
    },

    onBackBtn: function(){
        var me = this;
        me.fireEvent('onBackLogin');
    },
    
    onRecoveryBtn: function(){
        var me = this;
        var email = me.down('#emailId').getValue();
        
        var messageLabel = me.down('#messageLabel');
        messageLabel.hide();
        
        var task = Ext.create('Ext.util.DelayedTask', function () {
            messageLabel.setHtml('');
             me.fireEvent('recoveryPassword', me, email);
           // formulario.reset();
        });
        task.delay(500);
    },
    
    showMessage: function(mensaje) {
        var label = this.down('#messageLabel');
        label.setHtml(mensaje);
        label.show();
    }
});