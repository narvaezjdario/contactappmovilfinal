Ext.define("ContactApp.view.Contacto",{
    extend: 'Ext.Container',
    xtype: 'contacto',
    idCompany: null,
    requires: ['Ext.form.FieldSet', 'Ext.form.Password', 'Ext.Label', 'Ext.util.DelayedTask', 'Ext.Map'],
    config: {
        title: "Login",
        layout: 'vbox',
        items: [
            {
                xtype: 'toolbar',
                docked: 'top',
                title: 'Contacto',
                items:[
                    {
                        xtype: 'button',
                        iconCls: 'arrow_left',
                        ui: 'plain',
                        itemId: 'backBtn'
                    }
                ]

            },
            {
                flex: 1,
                xtype: 'map',
                mapOptions: {
                   // mapTypeId: new google.maps.MapTypeId.ROADMAP,
                    zoom: 16
                },
                useCurrentLocation: true
            },
            {
                flex: 3,
                xtype: 'formpanel',
                itemId: 'formContacto',
                items:[
                    {
                        xtype: 'fieldset',
                        items:[
                            {
                                xtype: 'label', html: 'Error en el formulario', 
                                hidden: true, //Campo oculto, para errores en el login 
                                style: 'color:#990000;margin:5px 0px;',
                                itemId: 'failedLabel'
                            },
                            {
                                name: 'contactType',
                                xtype: 'selectfield',
                                label: 'Tipo',
                                usePicker: true,
                                valueField: 'id',
                                displayField: 'name',
                                store: 'ContactTypes',
                                itemId: 'typeSelect',
                                autoSelect: false,
                                placeHolder: 'Seleccione una Tipo',
                                required:true
                            },
                            {
                                name: 'contactSubType',
                                xtype: 'selectfield',
                                label: 'SubTipo',
                                hidden: true,
                                usePicker: true,
                                valueField: 'id',
                                displayField: 'name',
                                itemId: 'subtypeSelect'
                            },
                            {
                                name: 'description',
                                xtype: 'textareafield',
                                label: 'Descripci&oacute;n',
                                placeHolder: 'Escriba aca la descripcion',
                                required:true,
                                itemId: 'description'

                            },
                            {
                                name: 'reference',
                                xtype: 'textfield',
                                label: 'Referencia',
                                placeHolder: '# linea de referencia',
                                required: true,
                                itemId: 'reference'
                            },
                            {
                                name: 'address',
                                xtype: 'textfield',
                                label: 'Direcci&oacute;n',
                                placeHolder: 'Direccion domiciliaria',
                                required: true,
                                itemId: 'address'
                            },

                            {
                                xtype: 'image',
                                src: "http://placehold.it/200x200",
                                width: 200,
                                height: 200,
                                style: 'margin: 1em auto;'
                            },
                            {
                                xtype: 'button',
                                text: 'Adicionar Fotograf&iacute;a',
                                itemId: 'cameraId'
                            }


                        ]
                    },
                    {
                        xtype: 'toolbar',
                        layout: {
                            pack: 'center' 
                        },
                        ui: 'plain',
                        items:[
                            {
                                xtype: 'button', text: 'Enviar', ui: 'action', itemId: 'guardarBtn'
                            }
                        ]
                    }
                ]
            }
        ],

        listeners: [
            {
                delegate: '#backBtn',
                event: 'tap',
                fn: 'onBackBtn'
            },
            {
                delegate: '#typeSelect',
                event: 'change',
                fn: 'onTypeSelect'    
            },
            {
                delegate: '#cameraId', 
                event: 'tap', 
                fn: 'onCamera'
            },
            {
                delegate: '#guardarBtn', 
                event: 'tap', 
                fn: 'onGuardar'
            }

        ]
    },

    initialize: function(){
        var me = this;
        me.callParent(arguments);
        this.initMap();
        Ext.getStore('ContactTypes').load();
        Ext.getStore('ContactSubTypes').load();
    },

    onBackBtn: function(){
        var me = this;
        me.fireEvent('onBackBtn');
    },

    onTypeSelect: function(selectbox,newValue,oldValue){
        var me = this;
        var subtypeSelect= me.down('#subtypeSelect');
        var typeSelect= me.down('#typeSelect');
        var contactSubTypesStore = Ext.getStore('ContactSubTypes').load();
        var contactSubTypes = [];
        
         contactSubTypesStore.each(function(rec) {
            if (typeSelect.getValue() === rec.data.contactTypeId ) {
                contactSubTypes.push(rec);
            }
        });
       

        var storeTemporal = Ext.create('Ext.data.Store', {
            model: 'ContactApp.model.ContactSubType',
            data: contactSubTypes
        });

        if(storeTemporal.data.length != 0){
            subtypeSelect.setStore(storeTemporal);
            subtypeSelect.show();
        }
        else{
            console.log('debe ocultarse');
            subtypeSelect.hide();
        }

    },

    onCamera: function(){
        var me = this;
        navigator.camera.getPicture(
            me.successPhoto,
            me.failPhoto,
            {
                quality: 50,
                destinationType: navigator.camera.DestinationType.DATA_URL,
                sourceType: navigator.camera.PictureSourceType.CAMERA
            }
        );
    },

    successPhoto: function(image_uri) {
        var me = this;
        var img = Ext.ComponentQuery.query("image")[1];
        img.setSrc("data:image/jpeg;base64," + image_uri);
        var foto = "data:image/jpeg;base64," + image_uri;
        localStorage.setItem("photoData",foto);
        console.log(localStorage.getItem("photoData"));
    },

    failPhoto: function(message) {
        alert("Failed: " + message);
    },

    onGuardar: function(){
        var me = this;
        var form = me.down('#formContacto');
        var failedLabel = me.down('#failedLabel');
        failedLabel.hide();
        var task = Ext.create('Ext.util.DelayedTask', function () {
            failedLabel.setHtml('');
            me.fireEvent('onGuardar',form.getValues(), me.idCompany);
        });

        task.delay(500);

    },

    showFailedMessage: function(mensaje) {
        var label = this.down('#failedLabel');
        label.setHtml(mensaje);
        label.show();
    },

    resetForm: function(){
        var formulario = this.down('#formContacto');
        formulario.setValues({
            contactType: '',
            contactSubType: '',
            description: '',
            reference: '',
            address: ''
        });
        var img = Ext.ComponentQuery.query("image")[0];
        img.setSrc("http://placehold.it/200x200");

    },

    addCompany: function(idCompany){
        var me = this;
        me.idCompany = idCompany;

        console.log('entro a addCompany con el Id: ');
        console.log(me.idCompany);
    },

    initMap: function(){

        var mapPanel = this.down('map');
        var gMap = mapPanel.getMap();

//        var panoramioLayer = new google.maps.panoramio.PanoramioLayer();
//        panoramioLayer.setTag("contactapp");
//        panoramioLayer.setMap(gMap);
//
//        var marker = new google.maps.Marker({
//            map: gMap,
//            animation: google.maps.Animation.DROP,
//            position: new google.maps.LatLng(138.600941, -34.927713)
//        });
        
        

    }

});
