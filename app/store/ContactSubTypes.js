Ext.define("ContactApp.store.ContactSubTypes", {
    extend: "Ext.data.Store",
    requires: ["ContactApp.model.Category", "Ext.data.proxy.Rest"    ],
    config: {
        autoLoad: false, //Cambiar a False luego de que se agrege la opción de login
        autoSync: true,
        model: "ContactApp.model.ContactSubType", 
        proxy: {
                type: "rest",
                disableCaching: false,
                withCredentials:true,
                //url: 'http://www.talentmp.com/contactapp/'+'api/contactSubType',
                url: 'http://192.168.0.15:8080/contactappserver/'+'api/contactSubType',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                }
            }
    }
});