Ext.define("ContactApp.store.Companies", {
    extend: "Ext.data.Store",
    requires: ["ContactApp.model.Company", "Ext.data.proxy.Rest"    ],
    config: {
        autoLoad: false, //Cambiar a False luego de que se agrege la opción de login
        autoSync: true,
        model: "ContactApp.model.Company", 
        proxy: {
                type: "rest",
                disableCaching: false,
                withCredentials:true,
                url: 'http://192.168.0.15:8080/contactappserver/'+'api/company',
                //url: 'http://www.talentmp.com/contactapp/'+'api/company',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                }
            }
    }
});