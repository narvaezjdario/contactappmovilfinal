Ext.define("ContactApp.store.Cities", {
    extend: "Ext.data.Store",
    requires: ["ContactApp.model.City", "Ext.data.proxy.Rest"    ],
    config: {
        autoLoad: true, //Cambiar a False luego de que se agrege la opción de login
        autoSync: true,
        model: "ContactApp.model.City", 
        proxy: {
            type: "rest",
            disableCaching: false,
            withCredentials:true,
            //url: 'http://www.talentmp.com/contactapp/'+'api/city',
            url: 'http://192.168.0.15:8080/contactappserver/'+'api/city',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        }
    }
});