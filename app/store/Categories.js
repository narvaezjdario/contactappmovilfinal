Ext.define("ContactApp.store.Categories", {
    extend: "Ext.data.Store",
    requires: ["ContactApp.model.Category", "Ext.data.proxy.Rest"    ],
    config: {
        autoLoad: false, //Cambiar a False luego de que se agrege la opción de login
        autoSync: true,
        model: "ContactApp.model.Category", 
        proxy: {
            type: "rest",
            disableCaching: false,
            withCredentials:true,
           //url: 'http://www.talentmp.com/contactapp/'+'api/category',
            url: 'http://192.168.0.15:8080/contactappserver/'+'api/category',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        }
    }
});