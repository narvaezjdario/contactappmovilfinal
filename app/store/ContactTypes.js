Ext.define("ContactApp.store.ContactTypes", {
    extend: "Ext.data.Store",
    requires: ["ContactApp.model.Category", "Ext.data.proxy.Rest"    ],
    config: {
        autoLoad: false, //Cambiar a False luego de que se agrege la opción de login
        autoSync: true,
        model: "ContactApp.model.ContactType", 
        proxy: {
            type: "rest",
            disableCaching: false,
            withCredentials:true,
            url: 'http://192.168.0.15:8080/contactappserver/'+'api/contactType',
            //url: 'http://www.talentmp.com/contactapp/'+'api/contactType',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        }
    }
});