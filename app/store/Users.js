Ext.define("ContactApp.store.Users", {
    extend: "Ext.data.Store",
    requires: ["ContactApp.model.User", "Ext.data.proxy.Rest"    ],
    config: {
        autoLoad: true, //Cambiar a False luego de que se agrege la opción de login
        autoSync: true,
        model: "ContactApp.model.User", 
        proxy: {
                type: "rest",
                disableCaching: false,
                withCredentials:true,
                //url: 'http://www.talentmp.com/contactapp/'+'api/user',
                url: 'http://192.168.0.15:8080/contactappserver/'+'api/user',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                }
            }
    }
});