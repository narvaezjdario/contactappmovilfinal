Ext.define('ContactApp.controller.Category', {
    extend: 'Ext.app.Controller',

    config:{
        refs: {
            categoryView: 'category',
            companyView: 'company',
            infoView: 'info',
            profileView: 'profile',
            historyView: 'history',
            createCategoryView: 'createcategory'
        },
        control:{
            categoryView: {
                onCategorySelected: 'onCategorySelected',
                onInfo: 'onInfo',
                onProfile: 'onProfile',
                onHistory: 'onHistory'
            },
            createCategoryView: {
                onCreateCategory: 'onCreateCategory'
            }
        }
    },

    onCategorySelected: function(category){
        var me = this;

        if(category.data.id === 2){
            console.log('selecciono la categoría otro');

            if(me.getCreateCategoryView() === undefined)
                var vistaCreateCategory = Ext.create('ContactApp.view.CreateCategory');
            else
                var vistaCreateCategory = me.getCreateCategoryView();

            Ext.Viewport.animateActiveItem(Ext.Viewport.add(vistaCreateCategory), {
                type: 'slide', direction: 'left'
            });


        }
        else{

            var companies = Ext.getStore('Companies');

            var companiesCategory = [];

            companies.each(function(rec) {
                if (category.data.id === rec.data.categoryId ) {
                    companiesCategory.push(rec);
                }
            });


            var storeTemporal = Ext.create('Ext.data.Store', {
                model: 'ContactApp.model.Company',
                data: companiesCategory
            });


            if(me.getCompanyView() === undefined)
                var vistaCompany = Ext.create('ContactApp.view.Company');
            else
                var vistaCompany = me.getCompanyView();

            vistaCompany.refreshCompanies(storeTemporal);

            Ext.Viewport.animateActiveItem(Ext.Viewport.add(vistaCompany), {
                type: 'slide', direction: 'left'
            });

        }

    },

    onInfo: function(){
        var me = this;

        if(me.getInfoView() === undefined)
            var vistaInfo = Ext.create('ContactApp.view.Info');
        else
            var vistaInfo = me.getInfoView();

        Ext.Viewport.animateActiveItem(Ext.Viewport.add(vistaInfo), {
            type: 'fade', direction: 'down'
        });

        Ext.Viewport.hideMenu('left');
    },

    onProfile: function(){
        var me = this;

        if(me.getProfileView() === undefined)
            var vistaProfile = Ext.create('ContactApp.view.Profile');
        else
            var vistaProfile = me.getProfileView();

        Ext.Viewport.animateActiveItem(Ext.Viewport.add(vistaProfile), {
            type: 'fade', direction: 'down'
        });

        Ext.Viewport.hideMenu('left');
    },

    onHistory: function(){
        var me = this;

        if(me.getHistoryView() === undefined)
            var vistaHistory = Ext.create('ContactApp.view.History');
        else
            var vistaHistory = me.getHistoryView();


        Ext.Ajax.request({
            url: 'http://192.168.0.15:8080/contactappserver/'+'api/contact/history/'+localStorage.getItem("userId"),
//            url: 'http://www.talentmp.com/contactapp/'+'api/contact/history/'+localStorage.getItem("userId"),
            method: 'get',
            timeout: 5000,
            headers: { 'Content-Type': 'application/json' , 'accept': 'application/json'},
            success: function(response){
                var validacion= response.status;
                var responseJson = Ext.JSON.decode(response.responseText);
                if (validacion === 200) {
                    console.log('Se hizo la petición de historial correctamente');
                    console.log(responseJson);
                    me.historySuccess(vistaHistory, responseJson);
                }
                else{
                    console.log('Fue respondido pero hubo un error en el servidor');
                }
            },
            failure: function(){
                console.log('Error al hacer la petición de historial');
            }
        });


    },

    historySuccess: function(vistaHistory, responseJson){
        var storeTemporal = Ext.create('Ext.data.Store', {
            model: 'ContactApp.model.Contact',
            data: responseJson
        });


        vistaHistory.refreshList(storeTemporal);

        Ext.Viewport.animateActiveItem(Ext.Viewport.add(vistaHistory), {
            type: 'fade', direction: 'down'
        });

        Ext.Viewport.hideMenu('left');
    },

    onCreateCategory: function(name){
        var me = this;
        var vistaCreateCategory = me.getCreateCategoryView();
        var categoryInstance = Ext.create('ContactApp.model.Category', {
            name: name
        });

        Ext.Ajax.request({
 //           url: 'http://www.talentmp.com/contactapp/'+'api/category',
            url: 'http://192.168.0.15:8080/contactappserver/'+'api/category',
            method: 'post',
            timeout: 5000,
            headers: { 'Content-Type': 'application/json' , 'accept': 'application/json'},
            jsonData: categoryInstance.data,
            success: function(response){
                var loginValidation = response.status;
                if (loginValidation === 201) {
                    console.log('La categoría fue creada exitosamente');
                    vistaCreateCategory.showMessage('Categoría creada exitosamente');
                }
                else{
                    console.log('Error al crear la categoría');
                    vistaCreateCategory.showMessage('Error al crear la categoría');
                }
            },
            failure: function(){
                console.log('Error en la petición de creación de Categoría');
                vistaCreateCategory.showMessage('Hubo un error al hacer la petición');
            }
        });
    }



});