Ext.define('ContactApp.controller.Share', {
    extend: 'Ext.app.Controller',

    config:{
        refs: {
            categoryView: 'category',
            shareView: 'share',
            infoView: 'info',
            profileView: 'profile',
            historyView: 'history',
            createCategoryView: 'createcategory'
        },
        control:{
            shareView: {
                onBackBtn: 'onBackBtn'
            },
            infoView: {
                onBackBtn: 'onBackBtn'
            },
            profileView: {
                onBackBtn: 'onBackBtn'
            },
            historyView: {
                onBackBtn: 'onBackBtn'
            },
            createCategoryView: {
                onBackBtn: 'onBackBtn'
            }
        }
    },

    onBackBtn: function(){
        var me = this;
        Ext.Viewport.animateActiveItem(me.getCategoryView(), {
            type: 'slide', direction: 'right'
        });
    }

});