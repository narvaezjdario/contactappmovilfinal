Ext.define('ContactApp.controller.Facebook', {
    extend: 'Ext.app.Controller',

    config:{
        refs: {
            loginView: 'login'
        },
        control:{
            loginView: {
                onFacebookBtnTap: 'onFacebookBtnTap'
            }
        }
    },

    onFacebookBtnTap: function(vista){
        var me = this;
        facebookConnectPlugin.login(["public_profile","email"],
                                    function(response){me.successLoginFb(response, vista);}, 
                                    function(error){me.errorLoginFb(error);}
                                   );
    },



    successLoginFb: function(response, vista){
        
        var me = this;
        var vistaLogin = me.getLoginView();
        var userType = 1;
        Ext.Ajax.request({
            url: 'http://192.168.0.15:8080/contactappserver/'+'auth/checkUserFb',
            //url: 'http://www.talentmp.com/contactapp/'+'auth/checkUserFb',
            method: 'post',
            disableCaching: false, 
            withCredentials:true, 
            useDefaultXhrHeader: false, 
            jsonData: {
                "fbid" : response.authResponse.userID
            },
            success: function(respuesta){
                me.getApplication().getController('ContactApp.controller.Auth').onLoginBtnTap(vistaLogin, response.authResponse.userID, "fblogin", userType);

            },
            failure: function(error){
                facebookConnectPlugin.api('/me', ['public_profile', 'email'], 
                                          function (result){
                    //alert("el tipo es: "+ typeof result);
                    result.token = response.authResponse.accessToken;
                    //alert("token: "+result.token);
                    var resultado = result;

                    me.getApplication().getController('ContactApp.controller.User').crearUsuario(me.getLoginView(),
                                                                                                 resultado);
                },
                                          function (error){alert("Failed: " + error);}
                                         );  

            }
        });

        //        //alert('respuesta de sucess: '+ JSON.stringify(response.authResponse.accessToken));


    },

    errorLoginFb: function(error){
        alert("error login facebook: " + error); 
    }

});