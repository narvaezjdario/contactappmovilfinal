Ext.define('ContactApp.controller.Contacto', {
    extend: 'Ext.app.Controller',

    config:{
        refs: {
            companyView: 'company',
            contactoView: 'contacto',
            shareView: 'share',
            historyView: 'history',
            contactDetailView: 'contactdetail'
        },
        control:{
            contactoView: {
                onBackBtn: 'onBackBtn',
                onTypeSelect: 'onTypeSelect',
                onGuardar: 'onGuardar'
            },
            
            historyView: {
                onContactSelected: 'onContactSelected'
            },
            
            contactDetailView: {
                onBackBtn: 'onBackBtnList'
            }
        }
    },


    onBackBtn: function(){
        var me = this;
        Ext.Viewport.animateActiveItem(me.getCompanyView(), {
            type: 'slide', direction: 'right'
        });
    },

    onTypeSelect: function(id){
        var contactSubTypes = Ext.getStore('ContactSubTypes').findRecord('contactTypeId', id);
        
        var storeTemporal = Ext.create('Ext.data.Store', {
            model: 'ContactApp.model.ContactSubType',
            data: contactSubTypes
        });

        return storeTemporal;
    },

    onGuardar: function(contacto, idCompany){
        var me = this;
        var contactInstance = Ext.create('ContactApp.model.Contact', contacto);
        contactInstance.set('image', localStorage.getItem("photoData"));
        contactInstance.set('contactSubType', (contacto.contactSubType!=null)?contacto.contactSubType:1);
        var errores = contactInstance.validate();
        
        
        console.log('Errores del formulario?');
        console.log(errores);
                
        if(errores.isValid()){
            me.createContact(contactInstance.data, idCompany);
        }
        else{
            me.getContactoView().showFailedMessage('Hubo error en el formulario');
        }
    },
    
    createContact: function(contacto, idCompany){
        console.log('antes de enviar el ajax idCompany: ');
        console.log(idCompany);
        var me = this;
        Ext.Ajax.request({
//            url: 'http://www.talentmp.com/contactapp/'+'api/contact',
            url: 'http://192.168.0.15:8080/contactappserver/'+'api/contact',
            method: 'post',
            timeout: 5000,
            noCache: true,
            withCredentials:true,
            headers: { 'Content-Type': 'application/json' , 'accept': 'application/json'},
            jsonData: {
                "class": "co.agilcon.contactapp.Contact",
                "address": contacto.address,
                "company": {
                    "class": "Company",
                    "id": idCompany
                },
                "contactSubType": {
                    "class": "ContactSubType",
                    "id": contacto.contactSubType
                },
                "contactType": {
                    "class": "ContactType",
                    "id": contacto.contactType
                },
                "description": contacto.description,
                "image": localStorage.getItem("photoData"),
                "reference": contacto.reference,
                "status": {
                    "class": "Param",
                    "id": 1
                },
                "type": 1,
                "user": {
                    "class": "User",
                    "id": localStorage.getItem("userId")
                }


            },
            success: function (response) {
                console.log('envio exitoso');
                if(response.status===201){
                    //localStorage.removeItem("photoData");
                    me.getContactoView().resetForm();
                    me.changeToShare();
                   // Ext.Msg.alert('Envio', 'Su solicitud fue procesada con Exito');
                }
                else{
                    Ext.Msg.alert('Petición enviada', 'La solicitud fue enviada pero no se completo');
                }
                    
            },
            failure: function (response) {
                Ext.Msg.alert('Fracaso envío', 'No fue posible resolver la petición');
            } 
        });
    },
    
    changeToShare: function(){
        var me = this;
        if(me.getShareView() === undefined)
            var vistaShare = Ext.create('ContactApp.view.Share');
        else
            var vistaShare = me.getShareView();
        
        Ext.Viewport.animateActiveItem(Ext.Viewport.add(vistaShare), {
            type: 'slide', direction: 'left'
        });
    },
    
    onContactSelected: function(contact){
        var me = this;
        if(me.getContactDetailView() === undefined)
            var vistaContactDetail = Ext.create('ContactApp.view.ContactDetail');
        else
            var vistaContactDetail = me.getContactDetailView();
        
        vistaContactDetail.refreshInfo(contact);
        
        Ext.Viewport.animateActiveItem(Ext.Viewport.add(vistaContactDetail), {
            type: 'slide', direction: 'left'
        });
    },
    
    onBackBtnList: function(){
        var me = this;
        Ext.Viewport.animateActiveItem(me.getHistoryView(), {
            type: 'slide', direction: 'right'
        });
    }
    

});