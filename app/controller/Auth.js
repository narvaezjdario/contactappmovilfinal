Ext.define('ContactApp.controller.Auth', {
    extend: 'Ext.app.Controller',

    config:{
        refs: {
            loginView: 'login',
            categoryView: 'category',
            recoveryView: 'recovery',
            registerView: 'register'
        },
        control:{
            loginView: {
                onLoginBtnTap: 'onLoginBtnTap',
                onRecoverBtnTap: 'onRecoverBtnTap'
            },
            categoryView:{
                onCerrar: 'onCerrar'
            },
            recoveryView:{
                recoveryPassword: 'recoveryPassword',
                onBackLogin: 'onBackLogin'
            },
            registerView:{
                recoveryPassword: 'recoveryPassword',
                onBackLogin: 'onBackLogin'
            }
            

        }
    },

    onLoginBtnTap: function(vistaLogin, username, password, userType){
        var me = this;
        if(username.length === 0 || password.length === 0){
            vistaLogin.showFailedMessage('Por favor ingresar username y password');
            return;
        }

        vistaLogin.setMasked({
            xtype: 'loadmask',
            message: 'Verificando'
        });

        Ext.Ajax.request({
            url: 'http://192.168.0.15:8080/contactappserver/'+'auth/signIn',
            //url: 'http://www.talentmp.com/contactapp/'+'auth/signIn',
            method: 'post',
            disableCaching: false, //Para eliminar unos parametros que agrega sencha por defecto
            withCredentials:true, //Para almacenar la cookie
            useDefaultXhrHeader: false, //Para enviar una solicitud CORS
            params: {
                mobile: true
                //username: username,
                //password: password
            },
            jsonData: {
                "username" : username,
                "password" : password,
                "userType" : userType
            },
            success: function(response){
                console.log(response.status);
                var loginResponse = Ext.JSON.decode(response.responseText);
                console.log(loginResponse.logueo);
                var loginValidation = loginResponse.loginResponse.status;
                if(loginValidation === '0'){
                    localStorage.setItem("userId",loginResponse.loginResponse.userId);
                    console.log('idUsuario');
                    console.log(localStorage.getItem("userId"));
                    me.signInSucess();
                }
                else{
                    me.signInFailure('Hubo un error en las credenciales. Por favor intenta de nuevo');
                    vistaLogin.setMasked(false);
                }
            },
            failure: function(response){
                me.signInFailure('Hubo un error en la conexión. Por favor intenta más tarde');
                vistaLogin.setMasked(false);
            }
        });
    },

    signInSucess: function(){
        var vistaLogin = this.getLoginView();
        //Ext.getStore('Categories').load();
        var vistaCategory= Ext.Viewport.add(Ext.create('ContactApp.view.Category'));
        vistaLogin.setMasked(false);
        Ext.Viewport.animateActiveItem(vistaCategory, {
            type: 'slide', direction: 'right'
        });

    },

    signInFailure: function(mensaje){
        var vistaLogin = this.getLoginView();
        vistaLogin.setMasked(false);
        vistaLogin.showFailedMessage(mensaje);
    },


    onCerrar: function(vista){

        var me = this;
        vista.setMasked({
            xtype: 'loadmask',
            message: 'Verificando'
        });

        Ext.Ajax.request({
            url: 'http://192.168.0.15:8080/contactappserver/'+'auth/signOut',
            //url: 'http://www.talentmp.com/contactapp/'+'auth/signOut',
            method: 'post',
            disableCaching: false, 
            withCredentials:true, 
            useDefaultXhrHeader: false, 
            success: function(response){
                localStorage.removeItem("userId");
                vista.setMasked(false);
                Ext.Viewport.animateActiveItem(me.getLoginView(), {
                    type: 'fade', direction: 'down'
                });

            },
            failure: function(response){
                vista.setMasked(false);
                Ext.Msg.alert('Error', 'Hubo un error en el cierre de Sesión');
            }
        });
    },


    onRecoverBtnTap: function(){
        var me = this;
        var vistaRecovery = null;
        
        if(me.getRecoveryView() === undefined)
            vistaRecovery = Ext.create('ContactApp.view.Recovery');
        else
            vistaRecovery = me.getRecoveryView();

        Ext.Viewport.animateActiveItem(Ext.Viewport.add(vistaRecovery), {
            type: 'fade', direction: 'down'
        });
    },

    recoveryPassword: function(vista, email){
        var me = this;
        vista.setMasked({
            xtype: 'loadmask',
            message: 'Verificando'
        });
        console.log(email);
        if(email != null){

            Ext.Ajax.request({
                url: 'http://192.168.0.15:8080/contactappserver/'+'auth/restorePassword',
                //url: 'http://www.talentmp.com/contactapp/'+'auth/restorePassword',
                method: 'post',
                disableCaching: false, 
                withCredentials:true, 
                useDefaultXhrHeader: false,
                jsonData: {
                    "email" : email
                },
                success: function(response){
                    var respuesta = Ext.JSON.decode(response.responseText);
                    console.log(respuesta.restorePassword.status);
                    console.log(respuesta.restorePassword.message);
                    
                    vista.showMessage(respuesta.restorePassword.message);
                    vista.setMasked(false);
                },
                failure: function(response){
                    vista.showMessage('error al enviar la petición');
                    vista.setMasked(false);

                }
            });

        }
        else{
            console.log('campo vacio');
        }
    },
    
    onBackLogin: function(){
        var me = this;
        Ext.Viewport.animateActiveItem(me.getLoginView(), {
            type: 'fade', direction: 'down'
        });
    }
    
    

});