var tokenPush = null;

Ext.define('ContactApp.controller.User', {
    extend: 'Ext.app.Controller',
    tokenPush: null,
    config:{
        refs: {
            loginView: 'login',
            registerView: 'register',
            profileView: 'profile'
        },
        control:{
            loginView: {
                onRegisterBtnTap: 'onRegisterBtnTap'
            },
            registerView: {
                crearUsuario: 'crearUsuario' 
            },
            profileView: {
                editarUsuario: 'editarUsuario'
            }
        }
    },



    onRegisterBtnTap: function(){


        //consulta token 
        var me = this;


        try{
            var viewRegister = Ext.Viewport.add(Ext.create('ContactApp.view.Register'));
            Ext.Viewport.animateActiveItem(viewRegister, {
                type: 'slide', direction: 'right'
            });
        }
        catch(error){
            Ext.Msg.alert('ERROR AL ABRIR LA VISTA REGISTRO', error);
        }
    },

    crearUsuario: function(vistaRegistro, usuario){
        var me = this;
        var userInstance = null;
        vistaRegistro.setMasked({
            xtype: 'loadmask',
            message: 'Verificando'
        });

        ContactApp.util.Pushwoosh.initPushwoosh();
        var pushNotification = window.plugins.pushNotification;  
        pushNotification.getPushToken(function(resp){
            me.tokenPush =""+resp;
            me.sendUser(vistaRegistro, usuario);
        });




    },

    editarUsuario: function(vistaEditar, usuario){

        console.log('entro a editar');
        var me = this;

        vistaEditar.setMasked({
            xtype: 'loadmask',
            message: 'Verificando'
        });

        var userInstance = Ext.create('ContactApp.model.User', usuario);
        //var userStore = Ext.getStore('Users');
        //var userInstance = userStore.findRecord('id', localStorage.getItem("userId"));

        //userInstance.set('');

        var errores = userInstance.validate();

        console.log('Errores del formulario?');
        console.log(errores);

        // userInstance.set(usuario);

        vistaEditar.setMasked(false);


        if(errores.isValid()){
            Ext.Ajax.request({
                //url: 'http://www.talentmp.com/contactapp/'+'api/user/'+localStorage.getItem("userId"),
               url: 'http://192.168.0.15:8080/contactappserver/'+'api/user/'+localStorage.getItem("userId"),
                method: 'put',
                timeout: 5000,
                headers: { 'Content-Type': 'application/json' , 'accept': 'application/json'},
                jsonData: userInstance.data,
                success: function(response){
                    var loginValidation = response.status;
                    if (loginValidation === 200) {
                        console.log('El usuario se edito correctamente');
                        vistaEditar.showOkMessage('Se registraron los cambios correctamente');
                        vistaEditar.setMasked(false);

                    }
                    else{
                        console.log('Error al editar el usuario');
                        vistaEditar.setMasked(false);
                        vistaEditar.showFailedMessage('Existio un error al editar el usuario');
                    }
                },
                failure: function(){
                    console.log('Error en la petición de edicion de Usuario');
                    vistaEditar.setMasked(false);
                    vistaEditar.showFailedMessage('No fue posible hacer la edición');
                }
            });
        }
        else{
            vistaEditar.setMasked(false);
            vistaEditar.showFailedMessage('Todos los campos son requeridos')
        }

    },

    sendUser: function(vistaRegistro, usuario){
        var me = this;
        var userInstance = null;

        if(usuario.verified){
            userInstance = Ext.create('ContactApp.model.UserFb', {
                fbid : usuario.id,
                fbtoken: usuario.token,
                username : usuario.email,
                name : usuario.name,
                email : usuario.email

            });
            userInstance.set('userType', 1);
        }
        else{
            userInstance = Ext.create('ContactApp.model.User', usuario);
            //Se agrega el campo de passwordHash porque fue quitado del modelo USER
            userInstance.data.passwordHash = usuario.passwordHash;
            userInstance.set('userType', 0);

            var cityInstance = Ext.create('ContactApp.model.City', {
                "class": "City",
                "id": usuario.city
            });

        }


        //var cityStore = Ext.getStore('Cities');
        //var cityInstance = cityStore.findRecord('id', usuario.city);

        //console.log('cityInstance: ');
        //console.log(cityInstance);





        userInstance.set('class', 'co.agilcon.contactapp.User');
        userInstance.set('id', 1);

        var errores = userInstance.validate();

        console.log('Errores del formulario?');
        console.log(JSON.stringify(errores));

        console.log('Ciudad: ');
        console.log(usuario.city);

        //para verificar que entra desde facebook




        //userInstance.data.city = cityInstance.data;
        userInstance.data.registrationToken= me.tokenPush;



        if(errores.isValid()){//&&usuario.city!=null){
            Ext.Ajax.request({
                //url: 'http://www.talentmp.com/contactapp/'+'api/user',
                url: 'http://192.168.0.15:8080/contactappserver/'+'api/user',
                method: 'post',
                timeout: 5000,
                headers: { 'Content-Type': 'application/json' , 'accept': 'application/json'},
                jsonData: userInstance.data,
                success: function(response){
                    var loginValidation = response.status;
                    if ((loginValidation === 201 )&& (userInstance.data.userType===0)) {
                        console.log('El usuario se registro correctamente');
                        vistaRegistro.clearForm();
                        vistaRegistro.setMasked(false);
                        me.getApplication().getController('ContactApp.controller.Auth').onLoginBtnTap(vistaRegistro, userInstance.data.username, userInstance.data.passwordHash, userInstance.data.userType);

                    }
                    else if ((loginValidation === 201 )&& (userInstance.data.userType===1)) {
                        console.log('El usuario se registro correctamente desde fb');
                        vistaRegistro.setMasked(false);
                        me.getApplication().getController('ContactApp.controller.Auth').onLoginBtnTap(vistaRegistro, userInstance.data.fbid, "fblogin", userInstance.data.userType);

                    }
                    else{
                        console.log('Error al crear el usuario');
                        vistaRegistro.setMasked(false);
                        vistaRegistro.showFailedMessage('Usuario ya Existente');
                    }
                },
                failure: function(){
                    console.log('Error en la petición de creación de Usuario');
                    vistaRegistro.setMasked(false);
                    vistaRegistro.showFailedMessage('No fue posible enviar el registro');
                }
            });
        }
        else{
            vistaRegistro.setMasked(false);
            vistaRegistro.showFailedMessage('Todos los campos son requeridos')
        }
    }


});