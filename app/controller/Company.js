Ext.define('ContactApp.controller.Company', {
    extend: 'Ext.app.Controller',

    config:{
        refs: {
            companyView: 'company',
            categoryView: 'category',
            contactoView: 'contacto'
        },
        control:{
            companyView: {
                onCompanySelected: 'onCompanySelected',
                onBackBtn: 'onBackBtn'
            }
        }
    },

    onCompanySelected: function(company){
        var me = this;
        if(me.getContactoView() === undefined)
            var vistaContacto = Ext.create('ContactApp.view.Contacto');
        else
            var vistaContacto = me.getContactoView();
        
        vistaContacto.addCompany(company.data.id);
        
        Ext.Viewport.animateActiveItem(Ext.Viewport.add(vistaContacto), {
            type: 'slide', direction: 'left'
        });
        
    },
    
    onBackBtn: function(){
        var me = this;
        Ext.Viewport.animateActiveItem(me.getCategoryView(), {
            type: 'slide', direction: 'right'
        });
    }

});